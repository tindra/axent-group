'use strict';

const activeClass  =   'is-active';
const fixedClass   =   'is-fixed';
const focusClass   =   'is-focused';
const hoverClass   =   'is-hover';
const disabledClass =  'is-disabled';
const visibleClass =   'is-visible';
const expandedClass =  'is-expanded';
const selectedClass =  'is-selected';
const collapsedClass = 'is-collapsed';
const scrolledClass =  'is-scrolled';
const headerScrolledClass = 'header-is-scrolled';
const lockedScrollClass = 'scroll-is-locked';
const mobileMenuVisibleClass = 'mobile-menu-is-visible';
const subnavVisibleClass = 'subnav-is-visible';
const sortingSelectVisible = 'sorting-select-is-visible';

const $header = $('[data-component="header"]');
const $body = $('.body');
const $main = $('.main');

const bpSM = 576;
const bpMD = 768;
const bpLG = 1024;
const bpXL = 1230;
const bp2XL = 1500;
const bp3XL = 1680;
/* Accordion */
$(document).ready(function(){
    let $accordionToggle = $('[data-element="accordion-toggle"]');

    $accordionToggle.on('click', function(e) {
        e.preventDefault();

        let $toggle = $(this);
        let $component = $toggle.closest('[data-component="accordion"]');
        let $item = $toggle.closest('[data-element="accordion-item"]');
        let $content = $item.find('[data-element="accordion-content"]');

        if ($item.hasClass(collapsedClass)) {
            $item.siblings().addClass(collapsedClass);
            $item.siblings().find('[data-element="accordion-content"]').slideUp(150);
            $content.slideDown(500);
            $item.removeClass(collapsedClass);
        } else {
            $content.slideUp(150);
            $item.addClass(collapsedClass);
        }
    });
});

// Amount form
$(document).ready(function() {
    $(document).on('click', function(e) {
        var target = $(e.target);
        var amountForm = "";

        if (e.target.dataset.component == "amount-form") {
            amountForm = target;
        } else if (target.parents('[data-component="amount-form"]').length) {
            amountForm = target.parents('[data-component="amount-form"]');
        }

        if (!amountForm) return;

        var
        amountInput = amountForm.find('[data-element="amount-input"]'),
        amountValue = amountInput.val(),
        amountValueNumber = +amountValue,
        amountMin = +amountInput.attr('data-min')  || 0,
        amountMax = +amountInput.attr('data-max'),
        amountStep = +amountInput.attr('data-step') || 1,
        amountNewValue = 0;

        amountInput.focus().val('').val(amountValue);

        if (target.attr('data-element') === 'inc-control' || target.attr('data-element') === 'dec-control') {
            if (target.attr('data-element') === 'inc-control') {
                amountForm.find('[data-element="dec-control"]').prop('disabled', false);
                if (amountMax && amountValueNumber >= amountMax) {
                    amountNewValue = amountValueNumber;
                    target.prop('disabled', true);
                } else {
                    amountNewValue = amountValueNumber + amountStep;
                }
                if (amountNewValue >= amountMax) {
                    target.prop('disabled', true);
                }
            } else if (target.attr('data-element') === 'dec-control') {
                amountForm.find('[data-element="inc-control"]').prop('disabled', false);
                if (amountValueNumber <= amountMin) {
                    amountNewValue = amountValueNumber;
                } else {
                    amountNewValue = amountValueNumber - amountStep;
                }
                if (amountNewValue <= amountMin) {
                    target.prop('disabled', true);
                }
            }
            if (Number.isInteger(amountStep)) {
                amountInput.val(amountNewValue);
            } else {
                amountInput.val(amountNewValue.toFixed(1));
            }
            amountInput.blur();
        } else {
            amountInput.focus().val('').val(amountValue);
        }
    });
});
/* Cleave inputs */
(function() {
    let $phoneInputs = document.querySelectorAll('[data-element="phone-input"]');

    if ($phoneInputs.length === 0) {
        return false;
    }

    for (const input of $phoneInputs) {
        let phoneCleave = new Cleave(input, {
            delimiters: ['+7 (', ')', ' ', '-', '-'],
            blocks: [0, 3, 0, 3, 2, 2],
            numericOnly: true
        });
    }
})();
/* Datepicker */
document.addEventListener('DOMContentLoaded', function() {
    let $datePickers = document.querySelectorAll('[data-element="datepicker"]');

    if ($datePickers) {
        for (var i = 0; i < $datePickers.length; i++) {
            let picker = datepicker($datePickers[i], {
                position: 'bl',
                startDay: 1,
                showAllDates: true,
                customDays: ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
                customOverlayMonths: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Aвг', 'Сен', 'Окт', 'Ноя', 'Дек'],
                customMonths: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Aвгуст', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                overlayButton: 'Выбрать',
                overlayPlaceholder: 'Год',
                formatter: (input, date, instance) => {
                    const value = date.toLocaleDateString();
                    input.value = value;
                },
                onSelect: (instance, date) => {
                    $(instance.el).parsley().validate();
                },
            });
        }
    }
});
/* Filters */
let $filters = $('[data-component="filters"]');
let $filtersTrigger = $('[data-element="filters-trigger"]');
let $filtersClose = $('[data-element="filters-close"]');

function hideFilters() {
    $filters.removeClass(visibleClass);
    $body.removeClass(lockedScrollClass);
}

function showFilters(component) {
    if ($(window).width() < bpXL) {
        component.addClass(visibleClass);
        $body.addClass(lockedScrollClass);
    }
}

function desktopCheck() {
    if ($(window).width() >= bpXL) {
        hideFilters();
    }
}

$(document).ready(function(){

    $filtersTrigger.on('click', function(e) {
        e.preventDefault();

        let $trigger = $(this);
        let $component = $('[data-component="filters"]');

        if ($component.hasClass(visibleClass)) {
            hideFilters();
        } else {
            showFilters($component);
        }
    });

    $filtersClose.on('click', function(e) {
        e.preventDefault();

        hideFilters();
    });

    $(document).on('keyup', function(e) {
        if ($(window).width() < bpXL) {
            if (e.key === "Escape" || e.keyCode === 27) {
                hideFilters();
            }
        }
    });

    $(document).on('click', function(e) {
        if ($(window).width() < bpXL) {
            if ($filters.has(e.target).length === 0 && !$filters.is(e.target) && $filtersTrigger.has(e.target).length === 0 && !$filtersTrigger.is(e.target)) {
                hideFilters();
            }
        }
    });

    desktopCheck();

    $(window).on('orientationchange resize', function() {
        desktopCheck();
    }); 
});
/* Float label */
document.addEventListener('DOMContentLoaded', function() {
    const formInputs = document.querySelectorAll('.form__field .form__input');

    formInputs.forEach(input => {

        input.addEventListener("focus", () => {
            const formGroup = input.closest('.form__field');

            formGroup.classList.add(focusClass); 
        });

        input.addEventListener("blur", () => {
            let inputValue = input.value;

            if (inputValue == '' ) {
                const formGroup = input.closest('.form__field');
                formGroup.classList.remove(focusClass);
            }
        });
    });
});
// Validation errors messages for Parsley
// Load this after Parsley

Parsley.addMessages('ru', {
    dateiso:  "Это значение должно быть корректной датой (ГГГГ-ММ-ДД).",
    minwords: "Это значение должно содержать не менее %s слов.",
    maxwords: "Это значение должно содержать не более %s слов.",
    words:    "Это значение должно содержать от %s до %s слов.",
    gt:       "Это значение должно быть больше.",
    gte:      "Это значение должно быть больше или равно.",
    lt:       "Это значение должно быть меньше.",
    lte:      "Это значение должно быть меньше или равно.",
    notequalto: "Это значение должно отличаться."
});

Parsley.addMessages('ru', {
    defaultMessage: "Некорректное значение.",
    type: {
        email:        "Введите адрес электронной почты.",
        url:          "Введите URL адрес.",
        number:       "Введите число.",
        integer:      "Введите целое число.",
        digits:       "Введите только цифры.",
        alphanum:     "Введите буквенно-цифровое значение."
    },
    notblank:       "Поле обязательно для заполнения",
    required:       "Поле обязательно для заполнения",
    pattern:        "Это значение некорректно.",
    min:            "Это значение должно быть не менее чем %s.",
    max:            "Это значение должно быть не более чем %s.",
    range:          "Это значение должно быть от %s до %s.",
    minlength:      "Это значение должно содержать не менее %s символов.",
    maxlength:      "Это значение должно содержать не более %s символов.",
    length:         "Это значение должно содержать от %s до %s символов.",
    mincheck:       "Выберите не менее %s значений.",
    maxcheck:       "Выберите не более %s значений.",
    check:          "Выберите от %s до %s значений.",
    equalto:        "Это значение должно совпадать."
});

Parsley.setLocale('ru');

/* Forms */
$(document).ready(function() {
    /* Select2 For parsley validation */
    $('[data-element="select"]').change(function() {
        $(this).trigger('input');
        $(this).parsley().validate();
    });

    /* Form Parsley Validation */
    $(function() {
        let $parsleyForm = $('[data-parsley-validate]');
        let $parsleyFormSubmit = $parsleyForm.find('input[type="submit"]');

        $parsleyForm.parsley({
            excluded: "[disabled], :hidden",
            errorClass: 'has-error',
            successClass: 'has-success',
            errorsWrapper: '<div class="form__errors-list"></div>',
            errorTemplate: '<div class="form__error"></div>',
            errorsContainer (field) {
                return field.$element.parent().closest('.form__input');
            },
            classHandler (field) {
                const $parent = field.$element.closest('.form__input');
                if ($parent.length) return $parent;

                return $parent;
            }
        });
    });

});
/* Header on scroll */
document.addEventListener('DOMContentLoaded', function() {
    (function () {
        const $header = document.querySelector('[data-component="header"]');
        const headerHeight = $header.offsetHeight;
        const $body = document.querySelector('.body');

        if (!$header) {
            return false;
        }

        let onScroll = debounce(function () {
            $header.classList.toggle(scrolledClass, window.scrollY > headerHeight);
            $body.classList.toggle(headerScrolledClass, window.scrollY > headerHeight);
        }, 0);

        onScroll();
        window.addEventListener('scroll', function() {
            onScroll();
        });
    })();
});
/* Hero Slider */
$(document).ready(function(){
    var $heroSlider = $('[data-slider="hero"]');

    $heroSlider.slick({
        dots: true,
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [
            {
                breakpoint: 1440,
                settings: {
                    arrows: false,
                }
            },
        ]
    });

    $(window).on('orientationchange resize', function() {
        $heroSlider.slick('resize');
    });
});

/* Mobile menu */
let $mobileMenu = $('[data-component="mobile-menu"]');
let $mobileMenuToggle = $('[data-element="mobile-menu-toggle"]');
let $mobileMenuClose = $('[data-element="mobile-menu-close"]');

function hideMobileMenu() {
    $mobileMenu.removeClass(visibleClass);
    $mobileMenuToggle.removeClass(activeClass);

    if ($body.hasClass(mobileMenuVisibleClass)) {
       $body.removeClass(lockedScrollClass);
       $body.removeClass(mobileMenuVisibleClass);
       $body.removeClass(subnavVisibleClass);
    }
}

function showMobileMenu(component) {
    if ($(window).width() < bpXL) {
        component.addClass(visibleClass);
        $mobileMenuToggle.addClass(activeClass);
        $body.addClass(mobileMenuVisibleClass);
        $body.addClass(lockedScrollClass);
    }
}

function desktopCheck() {
    if ($(window).width() >= bpXL) {
        hideMobileMenu();
    }
}

$(document).ready(function(){

    $mobileMenuToggle.on('click', function(e) {
        e.preventDefault();

        let $toggle = $(this);
        let $component = $('[data-component="mobile-menu"]');

        if ($component.hasClass(visibleClass)) {
            hideMobileMenu();
        } else {
            $('[data-component="mobile-menu"]').removeClass(visibleClass);
            showMobileMenu($component);
        }
    });

    $mobileMenuClose.on('click', function(e) {
        e.preventDefault();

        hideMobileMenu();
    });

    $(document).on('keyup', function(e) {
        if ($(window).width() < bpXL) {
            if (e.key === "Escape" || e.keyCode === 27) {
                hideMobileMenu();
            }
        }
    });

    $(document).on('click', function(e) {
        if ($(window).width() < bpXL) {
            if ($mobileMenu.has(e.target).length === 0 && !$mobileMenu.is(e.target) && $mobileMenuToggle.has(e.target).length === 0 && !$mobileMenuToggle.is(e.target) && $('[data-element="nav-close"]').has(e.target).length === 0 && !$('[data-element="nav-close"]').is(e.target)) {
                hideMobileMenu();
            }
        }
    });

    desktopCheck();

    $(window).on('orientationchange resize', function() {
        desktopCheck();
    }); 
});
/* Modal */
$(function() {
    $('[data-element="modal-trigger"]').magnificPopup({
        type: 'inline',
        fixedContentPos: true,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        callbacks: {
            beforeOpen: function() {
                let effectData = this.st.el.attr('data-effect');

                if (!effectData) {
                    effectData = 'mfp-zoom-in';
                }

                this.st.mainClass = effectData;
            },
            open: function() {
                $('.body').css('overflow', "hidden");
                let $mfp = $.magnificPopup.instance;
                let $trigger = $($mfp.currItem.el[0]);
                $trigger.attr('disabled', true);
            },
            close: function() {
                $('.body').css('overflow', "");
                let $mfp = $.magnificPopup.instance;
                let $trigger = $($mfp.currItem.el[0]);
                $trigger.removeAttr('disabled');
            }
        },
    });


    $('[data-element="control-modal-trigger"]').change(function() {
        let $control = $(this);

        if ($control.prop('checked')) {
            let mfpSrc = $control.attr('data-src');
            let mfpEffect = $control.attr('data-effect');

            $.magnificPopup.open({
                items: {
                    src: mfpSrc
                },
                type: 'inline',
                fixedContentPos: true,
                fixedBgPos: true,

                overflowY: 'auto',

                closeBtnInside: true,
                preloader: false,

                midClick: true,
                removalDelay: 300,
                callbacks: {
                    beforeOpen: function() {
                        let effectData = mfpEffect;

                        if (!effectData) {
                            effectData = 'mfp-zoom-in';
                        }

                        this.st.mainClass = effectData;
                    },
                    open: function() {
                        $('.body').css('overflow', "hidden");
                    },
                    close: function() {
                        $('.body').css('overflow', "");
                    }
                },
            });
        };
    });

    /* Gallery Modal */
    $('[data-element="gallery-trigger"]').magnificPopup({
        type: 'image',
        closeOnContentClick: false,
        mainClass: 'mfp-zoom-in',
        image: {
          verticalFit: true,
        },
        gallery: {
          enabled: true
        },
        zoom: {
          enabled: true,
          duration: 300, // don't foget to change the duration also in CSS
          opener: function(element) {
            return element.find('img');
          }
        },
        callbacks: {
            buildControls: function() {
                // re-appends controls inside the main container
                this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
            }
        }
    });

    $(document).on('click', '[data-element="modal-close"]', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
});

/* Move a block between it's mobile and desktop placeholders  */
$(document).ready(function(){
    moveBlocks();
    $(window).on('orientationchange resize', function() {
        moveBlocks();
    });

    function moveBlocks() {
        let $movableBlock = $('[data-block="movable-block"]');

        $movableBlock.each(function (index, item) {
            let $block = $(item);
            let blockID = $block.attr('data-id');
            let $blockPlaceholderMobile = $('[data-element="placeholder"][data-device="mobile"][data-id="' + blockID + '"]');
            let $blockPlaceholderTablet = $('[data-element="placeholder"][data-device="tablet"][data-id="' + blockID + '"]');
            let $blockPlaceholderDesktop = $('[data-element="placeholder"][data-device="desktop"][data-id="' + blockID + '"]');
            let blockBreakpoint = $block.attr('data-breakpoint');
            let windowWidth = $(window).width();
            let breakpoint = bpXL;

            if (blockBreakpoint) {
                breakpoint = blockBreakpoint;
            }

            if (windowWidth >= breakpoint) {
                if ($blockPlaceholderDesktop.length === 0) {
                    $block.appendTo($blockPlaceholderTablet);
                } else {
                    $block.appendTo($blockPlaceholderDesktop);
                }
            } else if (windowWidth >= bpLG && windowWidth < blockBreakpoint && $blockPlaceholderTablet.length > 0) {
                $block.appendTo($blockPlaceholderTablet);
            } else if (windowWidth < bpLG) {
                $block.appendTo($blockPlaceholderMobile);
            }
        });
    }
});

/* Nav */
$(function() {
    let $navToggle = $('[data-element="nav-toggle"]');
    let $navItems = $('[data-element="nav-item"]');
    let $navClose = $('[data-element="nav-close"]');

    $navClose.on('click', function(e){
        e.preventDefault();

        $navItems.removeClass(activeClass);
        $navItems.show();

        $body.removeClass(subnavVisibleClass);
    });

    navResponsive();
    $(window).on('orientationchange resize', function() {
        navResponsive();
    });

    function navResponsive() {
        if ($(window).width() < bpXL) {
            $navToggle.on('click', function(e){
                e.preventDefault();

                let $toggle = $(this);
                let $item = $toggle.closest('[data-element="nav-item"]');
                let $component = $toggle.closest('[data-component="nav"]');

                $item.toggleClass(activeClass);
                $item.siblings().toggle();

                $body.toggleClass(subnavVisibleClass);
            });
        } else {
            $navItems.removeClass(activeClass);
            $navItems.show();
        }
    }
});
/* Panel */
$(document).ready(function(){
    let $panelToggle = $('[data-element="panel-toggle"]');

    $panelToggle.on('click', function(e){
        e.preventDefault();

        let $toggle = $(this);
        let $panel = $toggle.closest('[data-component="panel"]');
        let $content = $panel.find('[data-element="panel-content"]');

        $panel.toggleClass(collapsedClass);
    });
});
/* Perfect scrollbar */
(function() {
    const psContainers = document.querySelectorAll('[data-perfect-scrollbar]');

    if (!psContainers) {
        return false;
    }

    for (const container of psContainers) {
        const ps = new PerfectScrollbar(container);
    }
})();

/* Product slider */
$(document).ready(function(){
    let $productSlider = $('[data-slider="product"]');

    $productSlider.slick({
        infinite: false,
        asNavFor: '[data-slider="product-thumbs"]',
    });

    let $productThumbs = $('[data-slider="product-thumbs"]');
    $productThumbs.slick({
        infinite: false,
        vertical: true,
        verticalSwiping: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        focusOnSelect: true,
        asNavFor: '[data-slider="product"]',
    });

    $(window).on('orientationchange resize', function() {
        $productSlider.slick('resize');
        $productThumbs.slick('resize');
    });
});
jQuery(document).ready(function($){
    let $rangeSlider = $('[data-element="range-slider"]');

    $rangeSlider.each(function(){
        let $range = $(this);
        let $rangeComponent = $range.closest('[data-component="range"]');
        let $rangeInputFrom = $rangeComponent.find('[data-element="range-input-from"]');
        let $rangeInputTo = $rangeComponent.find('[data-element="range-input-to"]');
        let instance;

        $range.ionRangeSlider({
            skin: 'round',
            type: "double",
            hide_min_max: true,
            hide_from_to: true,
            grid: false,
            min: $range.attr('data-min'),
            max: $range.attr('data-max'),
            step: $range.attr('data-step'),
            onStart: function(data) {
                $rangeInputFrom.prop("value", data.from);
                $rangeInputTo.prop("value", data.to);
            },
            onChange: function(data) {
                $rangeInputFrom.prop("value", data.from);
                $rangeInputTo.prop("value", data.to);
            }
        });

        instance = $range.data("ionRangeSlider");

        $rangeInputFrom.on("input", function() {
            let val = $(this).prop("value");

            // validate
            if (val < instance.result.min) {
                val = instance.result.min;
            } else if (val > instance.result.max) {
                val = instance.result.max;
            }

            instance.update({
                from: val
            });
        });

        $rangeInputTo.on("input", function() {
            let val = $(this).prop("value");

            // validate
            if (val > instance.result.max) {
                val = instance.result.max;
            } else if (val < instance.result.from) {
                val = instance.result.from;
            }

            instance.update({
                to: val
            });
        });
    });
});
/* Search */
$(document).ready(function(){
    let $search = $('[data-component="search"]');
    let $searchInput = $('[data-element="search-input"]');
    let $searchClear = $('[data-element="search-clear"]');

    $searchInput.on('input', function(e) {
        $search.fadeIn();
        $search.addClass(activeClass);
    });

    $searchClear.on('click', function(e) {
        e.preventDefault();

        $searchInput.val('');
        $search.removeClass(activeClass);
    });
});

/* Select2 */
$(document).ready(function() {
    let selectCommonOptions = {
        dropdownCssClass: ':all:',
        containerCssClass: ':all:',
        minimumResultsForSearch: Infinity,
        width: '100%',
    };

    $('[data-element="select"]').select2(selectCommonOptions);
});
/* Show More/Less */
$.fn.toggleText = function(t1, t2){
    if (this.text() == t1) {
        this.text(t2);
    } else {
        this.text(t1);
    }
    return this;
};

$(document).ready(function(){
    let $showAllToggle = $('[data-element="show-all-toggle"]');

    $showAllToggle.on('click', function(e) {
        e.preventDefault();

        let $toggle = $(this);
        let toggleTextOn = $toggle.attr('data-text-on');
        let toggleTextOff = $toggle.attr('data-text-off');
        let $showAllComponent = $toggle.closest('[data-component="show-all"]');

        $toggle.toggleText(toggleTextOn, toggleTextOff);

        $showAllComponent.find('[data-element="hidden-element"]').toggleClass('hidden');
    });
});

/* Slider Desktop */
$(document).ready(function(){
    var $sliderDesktop = $('[data-slider="slider-desktop"]');

    $sliderDesktop.slick({
        infinite: false,
        autoSlidesToShow: true,
        variableWidth: true,
        responsive: [
            {
                breakpoint: 1230,
                settings: 'unslick'
            },
        ]
    });

    $(window).on('orientationchange resize', function() {
        $sliderDesktop.slick('resize');
    });
});

/* Slider */
$(document).ready(function(){
    var $slider = $('[data-slider="slider"]');

    $slider.slick({
        autoSlidesToShow: true,
        variableWidth: true,
        responsive: [
            {
                breakpoint: 1230,
                settings: {
                    arrows: false,
                    dots: true,
                }
            },
        ]
    });

    $(window).on('orientationchange resize', function() {
        $slider.slick('resize');
    });
});

/*! smooth-scroll v12.1.5 | (c) 2017 Chris Ferdinandi | MIT License | http://github.com/cferdinandi/smooth-scroll */
!(function(e,t){"function"==typeof define&&define.amd?define([],(function(){return t(e)})):"object"==typeof exports?module.exports=t(e):e.SmoothScroll=t(e)})("undefined"!=typeof global?global:"undefined"!=typeof window?window:this,(function(e){"use strict";var t="querySelector"in document&&"addEventListener"in e&&"requestAnimationFrame"in e&&"closest"in e.Element.prototype,n={ignore:"[data-scroll-ignore]",header:null,speed:500,offset:0,easing:"easeInOutCubic",customEasing:null,before:function(){},after:function(){}},o=function(){for(var e={},t=0,n=arguments.length;t<n;t++){var o=arguments[t];!(function(t){for(var n in t)t.hasOwnProperty(n)&&(e[n]=t[n])})(o)}return e},a=function(t){return parseInt(e.getComputedStyle(t).height,10)},r=function(e){"#"===e.charAt(0)&&(e=e.substr(1));for(var t,n=String(e),o=n.length,a=-1,r="",i=n.charCodeAt(0);++a<o;){if(0===(t=n.charCodeAt(a)))throw new InvalidCharacterError("Invalid character: the input contains U+0000.");t>=1&&t<=31||127==t||0===a&&t>=48&&t<=57||1===a&&t>=48&&t<=57&&45===i?r+="\\"+t.toString(16)+" ":r+=t>=128||45===t||95===t||t>=48&&t<=57||t>=65&&t<=90||t>=97&&t<=122?n.charAt(a):"\\"+n.charAt(a)}return"#"+r},i=function(e,t){var n;return"easeInQuad"===e.easing&&(n=t*t),"easeOutQuad"===e.easing&&(n=t*(2-t)),"easeInOutQuad"===e.easing&&(n=t<.5?2*t*t:(4-2*t)*t-1),"easeInCubic"===e.easing&&(n=t*t*t),"easeOutCubic"===e.easing&&(n=--t*t*t+1),"easeInOutCubic"===e.easing&&(n=t<.5?4*t*t*t:(t-1)*(2*t-2)*(2*t-2)+1),"easeInQuart"===e.easing&&(n=t*t*t*t),"easeOutQuart"===e.easing&&(n=1- --t*t*t*t),"easeInOutQuart"===e.easing&&(n=t<.5?8*t*t*t*t:1-8*--t*t*t*t),"easeInQuint"===e.easing&&(n=t*t*t*t*t),"easeOutQuint"===e.easing&&(n=1+--t*t*t*t*t),"easeInOutQuint"===e.easing&&(n=t<.5?16*t*t*t*t*t:1+16*--t*t*t*t*t),e.customEasing&&(n=e.customEasing(t)),n||t},u=function(){return Math.max(document.body.scrollHeight,document.documentElement.scrollHeight,document.body.offsetHeight,document.documentElement.offsetHeight,document.body.clientHeight,document.documentElement.clientHeight)},c=function(e,t,n){var o=0;if(e.offsetParent)do{o+=e.offsetTop,e=e.offsetParent}while(e);return o=Math.max(o-t-n,0)},s=function(e){return e?a(e)+e.offsetTop:0},l=function(t,n,o){o||(t.focus(),document.activeElement.id!==t.id&&(t.setAttribute("tabindex","-1"),t.focus(),t.style.outline="none"),e.scrollTo(0,n))},f=function(t){return!!("matchMedia"in e&&e.matchMedia("(prefers-reduced-motion)").matches)};return function(a,d){var m,h,g,p,v,b,y,S={};S.cancelScroll=function(){cancelAnimationFrame(y)},S.animateScroll=function(t,a,r){var f=o(m||n,r||{}),d="[object Number]"===Object.prototype.toString.call(t),h=d||!t.tagName?null:t;if(d||h){var g=e.pageYOffset;f.header&&!p&&(p=document.querySelector(f.header)),v||(v=s(p));var b,y,E,I=d?t:c(h,v,parseInt("function"==typeof f.offset?f.offset():f.offset,10)),O=I-g,A=u(),C=0,w=function(n,o){var r=e.pageYOffset;if(n==o||r==o||(g<o&&e.innerHeight+r)>=A)return S.cancelScroll(),l(t,o,d),f.after(t,a),b=null,!0},Q=function(t){b||(b=t),C+=t-b,y=C/parseInt(f.speed,10),y=y>1?1:y,E=g+O*i(f,y),e.scrollTo(0,Math.floor(E)),w(E,I)||(e.requestAnimationFrame(Q),b=t)};0===e.pageYOffset&&e.scrollTo(0,0),f.before(t,a),S.cancelScroll(),e.requestAnimationFrame(Q)}};var E=function(e){h&&(h.id=h.getAttribute("data-scroll-id"),S.animateScroll(h,g),h=null,g=null)},I=function(t){if(!f()&&0===t.button&&!t.metaKey&&!t.ctrlKey&&(g=t.target.closest(a))&&"a"===g.tagName.toLowerCase()&&!t.target.closest(m.ignore)&&g.hostname===e.location.hostname&&g.pathname===e.location.pathname&&/#/.test(g.href)){var n;try{n=r(decodeURIComponent(g.hash))}catch(e){n=r(g.hash)}if("#"===n){t.preventDefault(),h=document.body;var o=h.id?h.id:"smooth-scroll-top";return h.setAttribute("data-scroll-id",o),h.id="",void(e.location.hash.substring(1)===o?E():e.location.hash=o)}h=document.querySelector(n),h&&(h.setAttribute("data-scroll-id",h.id),h.id="",g.hash===e.location.hash&&(t.preventDefault(),E()))}},O=function(e){b||(b=setTimeout((function(){b=null,v=s(p)}),66))};return S.destroy=function(){m&&(document.removeEventListener("click",I,!1),e.removeEventListener("resize",O,!1),S.cancelScroll(),m=null,h=null,g=null,p=null,v=null,b=null,y=null)},S.init=function(a){t&&(S.destroy(),m=o(n,a||{}),p=m.header?document.querySelector(m.header):null,v=s(p),document.addEventListener("click",I,!1),e.addEventListener("hashchange",E,!1),p&&e.addEventListener("resize",O,!1))},S.init(d),S}}));

var scroll = new SmoothScroll('[data-scroll]', {
	speed: 1000,
	easing: 'easeInOutCubic',
    header: '[data-component="header"]'
});
let $sortingToggle = $('[data-element="sorting-toggle"]');

$sortingToggle.on('click', function(e) {
    e.preventDefault();

    let $toggle = $(this);
    let $select = $('[data-element="select"][data-id="'+$toggle.attr('data-id')+'"]');

    $select.select2({
        dropdownParent: $toggle.parent(),
        dropdownCssClass: ':all:',
        containerCssClass: ':all:',
        minimumResultsForSearch: Infinity,
        width: 223,
    });

    $select.on('select2:select', function (e) {
        $body.removeClass(sortingSelectVisible);
    });

    if ($body.hasClass(sortingSelectVisible)) {
        $body.removeClass(sortingSelectVisible);
        $select.select2('close');
    } else {
        $body.addClass(sortingSelectVisible);
        $select.select2('open');
    }
});
/* Tabs */
$(document).ready(function(){
    let $tabToggle = $('[data-element="tabs-toggle"]');

    $tabToggle.on('click', function(e) {
        e.stopPropagation();

        let $toggle = $(this);
        let $tabs = $toggle.closest('[data-component="tabs"]');
        let $control = $toggle.find('input');
        let trigger = $toggle.attr('data-trigger');
        let tabID;


        if ($toggle.attr('href')) {
            tabID = $toggle.attr('href').slice(1);
        }

        if ($toggle.attr('data-src')) {
            tabID = $toggle.attr('data-src');
        }

        let $tabActive = '[data-element="tabs-tab"][data-id="' + tabID +'"]';

        if (!($toggle.hasClass(activeClass))) {
            $tabs.find('[data-element="tabs-tab"]').removeClass(activeClass);
            $tabs.find('.' + activeClass +'[data-element="tabs-toggle"]').removeClass(activeClass);
            $tabs.find($tabActive).addClass(activeClass);
            $toggle.addClass(activeClass);
        }
    });
});
