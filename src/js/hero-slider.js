/* Hero Slider */
$(document).ready(function(){
    var $heroSlider = $('[data-slider="hero"]');

    $heroSlider.slick({
        dots: true,
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [
            {
                breakpoint: 1440,
                settings: {
                    arrows: false,
                }
            },
        ]
    });

    $(window).on('orientationchange resize', function() {
        $heroSlider.slick('resize');
    });
});
