/* Panel */
$(document).ready(function(){
    let $panelToggle = $('[data-element="panel-toggle"]');

    $panelToggle.on('click', function(e){
        e.preventDefault();

        let $toggle = $(this);
        let $panel = $toggle.closest('[data-component="panel"]');
        let $content = $panel.find('[data-element="panel-content"]');

        $panel.toggleClass(collapsedClass);
    });
});