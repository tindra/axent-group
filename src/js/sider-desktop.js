/* Slider Desktop */
$(document).ready(function(){
    var $sliderDesktop = $('[data-slider="slider-desktop"]');

    $sliderDesktop.slick({
        infinite: false,
        autoSlidesToShow: true,
        variableWidth: true,
        responsive: [
            {
                breakpoint: 1230,
                settings: 'unslick'
            },
        ]
    });

    $(window).on('orientationchange resize', function() {
        $sliderDesktop.slick('resize');
    });
});
