/* Move a block between it's mobile and desktop placeholders  */
$(document).ready(function(){
    moveBlocks();
    $(window).on('orientationchange resize', function() {
        moveBlocks();
    });

    function moveBlocks() {
        let $movableBlock = $('[data-block="movable-block"]');

        $movableBlock.each(function (index, item) {
            let $block = $(item);
            let blockID = $block.attr('data-id');
            let $blockPlaceholderMobile = $('[data-element="placeholder"][data-device="mobile"][data-id="' + blockID + '"]');
            let $blockPlaceholderTablet = $('[data-element="placeholder"][data-device="tablet"][data-id="' + blockID + '"]');
            let $blockPlaceholderDesktop = $('[data-element="placeholder"][data-device="desktop"][data-id="' + blockID + '"]');
            let blockBreakpoint = $block.attr('data-breakpoint');
            let windowWidth = $(window).width();
            let breakpoint = bpXL;

            if (blockBreakpoint) {
                breakpoint = blockBreakpoint;
            }

            if (windowWidth >= breakpoint) {
                if ($blockPlaceholderDesktop.length === 0) {
                    $block.appendTo($blockPlaceholderTablet);
                } else {
                    $block.appendTo($blockPlaceholderDesktop);
                }
            } else if (windowWidth >= bpLG && windowWidth < blockBreakpoint && $blockPlaceholderTablet.length > 0) {
                $block.appendTo($blockPlaceholderTablet);
            } else if (windowWidth < bpLG) {
                $block.appendTo($blockPlaceholderMobile);
            }
        });
    }
});
