/* Accordion */
$(document).ready(function(){
    let $accordionToggle = $('[data-element="accordion-toggle"]');

    $accordionToggle.on('click', function(e) {
        e.preventDefault();

        let $toggle = $(this);
        let $component = $toggle.closest('[data-component="accordion"]');
        let $item = $toggle.closest('[data-element="accordion-item"]');
        let $content = $item.find('[data-element="accordion-content"]');

        if ($item.hasClass(collapsedClass)) {
            $item.siblings().addClass(collapsedClass);
            $item.siblings().find('[data-element="accordion-content"]').slideUp(150);
            $content.slideDown(500);
            $item.removeClass(collapsedClass);
        } else {
            $content.slideUp(150);
            $item.addClass(collapsedClass);
        }
    });
});
