/* Modal */
$(function() {
    $('[data-element="modal-trigger"]').magnificPopup({
        type: 'inline',
        fixedContentPos: true,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        callbacks: {
            beforeOpen: function() {
                let effectData = this.st.el.attr('data-effect');

                if (!effectData) {
                    effectData = 'mfp-zoom-in';
                }

                this.st.mainClass = effectData;
            },
            open: function() {
                $('.body').css('overflow', "hidden");
                let $mfp = $.magnificPopup.instance;
                let $trigger = $($mfp.currItem.el[0]);
                $trigger.attr('disabled', true);
            },
            close: function() {
                $('.body').css('overflow', "");
                let $mfp = $.magnificPopup.instance;
                let $trigger = $($mfp.currItem.el[0]);
                $trigger.removeAttr('disabled');
            }
        },
    });


    $('[data-element="control-modal-trigger"]').change(function() {
        let $control = $(this);

        if ($control.prop('checked')) {
            let mfpSrc = $control.attr('data-src');
            let mfpEffect = $control.attr('data-effect');

            $.magnificPopup.open({
                items: {
                    src: mfpSrc
                },
                type: 'inline',
                fixedContentPos: true,
                fixedBgPos: true,

                overflowY: 'auto',

                closeBtnInside: true,
                preloader: false,

                midClick: true,
                removalDelay: 300,
                callbacks: {
                    beforeOpen: function() {
                        let effectData = mfpEffect;

                        if (!effectData) {
                            effectData = 'mfp-zoom-in';
                        }

                        this.st.mainClass = effectData;
                    },
                    open: function() {
                        $('.body').css('overflow', "hidden");
                    },
                    close: function() {
                        $('.body').css('overflow', "");
                    }
                },
            });
        };
    });

    /* Gallery Modal */
    $('[data-element="gallery-trigger"]').magnificPopup({
        type: 'image',
        closeOnContentClick: false,
        mainClass: 'mfp-zoom-in',
        image: {
          verticalFit: true,
        },
        gallery: {
          enabled: true
        },
        zoom: {
          enabled: true,
          duration: 300, // don't foget to change the duration also in CSS
          opener: function(element) {
            return element.find('img');
          }
        },
        callbacks: {
            buildControls: function() {
                // re-appends controls inside the main container
                this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
            }
        }
    });

    $(document).on('click', '[data-element="modal-close"]', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
});
