/* Search */
$(document).ready(function(){
    let $search = $('[data-component="search"]');
    let $searchInput = $('[data-element="search-input"]');
    let $searchClear = $('[data-element="search-clear"]');

    $searchInput.on('input', function(e) {
        $search.fadeIn();
        $search.addClass(activeClass);
    });

    $searchClear.on('click', function(e) {
        e.preventDefault();

        $searchInput.val('');
        $search.removeClass(activeClass);
    });
});
