jQuery(document).ready(function($){
    let $rangeSlider = $('[data-element="range-slider"]');

    $rangeSlider.each(function(){
        let $range = $(this);
        let $rangeComponent = $range.closest('[data-component="range"]');
        let $rangeInputFrom = $rangeComponent.find('[data-element="range-input-from"]');
        let $rangeInputTo = $rangeComponent.find('[data-element="range-input-to"]');
        let instance;

        $range.ionRangeSlider({
            skin: 'round',
            type: "double",
            hide_min_max: true,
            hide_from_to: true,
            grid: false,
            min: $range.attr('data-min'),
            max: $range.attr('data-max'),
            step: $range.attr('data-step'),
            onStart: function(data) {
                $rangeInputFrom.prop("value", data.from);
                $rangeInputTo.prop("value", data.to);
            },
            onChange: function(data) {
                $rangeInputFrom.prop("value", data.from);
                $rangeInputTo.prop("value", data.to);
            }
        });

        instance = $range.data("ionRangeSlider");

        $rangeInputFrom.on("input", function() {
            let val = $(this).prop("value");

            // validate
            if (val < instance.result.min) {
                val = instance.result.min;
            } else if (val > instance.result.max) {
                val = instance.result.max;
            }

            instance.update({
                from: val
            });
        });

        $rangeInputTo.on("input", function() {
            let val = $(this).prop("value");

            // validate
            if (val > instance.result.max) {
                val = instance.result.max;
            } else if (val < instance.result.from) {
                val = instance.result.from;
            }

            instance.update({
                to: val
            });
        });
    });
});