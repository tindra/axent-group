'use strict';

const activeClass  =   'is-active';
const fixedClass   =   'is-fixed';
const focusClass   =   'is-focused';
const hoverClass   =   'is-hover';
const disabledClass =  'is-disabled';
const visibleClass =   'is-visible';
const expandedClass =  'is-expanded';
const selectedClass =  'is-selected';
const collapsedClass = 'is-collapsed';
const scrolledClass =  'is-scrolled';
const headerScrolledClass = 'header-is-scrolled';
const lockedScrollClass = 'scroll-is-locked';
const mobileMenuVisibleClass = 'mobile-menu-is-visible';
const subnavVisibleClass = 'subnav-is-visible';
const sortingSelectVisible = 'sorting-select-is-visible';

const $header = $('[data-component="header"]');
const $body = $('.body');
const $main = $('.main');

const bpSM = 576;
const bpMD = 768;
const bpLG = 1024;
const bpXL = 1230;
const bp2XL = 1500;
const bp3XL = 1680;