/* Slider */
$(document).ready(function(){
    var $slider = $('[data-slider="slider"]');

    $slider.slick({
        autoSlidesToShow: true,
        variableWidth: true,
        responsive: [
            {
                breakpoint: 1230,
                settings: {
                    arrows: false,
                    dots: true,
                }
            },
        ]
    });

    $(window).on('orientationchange resize', function() {
        $slider.slick('resize');
    });
});
