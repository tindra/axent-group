/* Float label */
document.addEventListener('DOMContentLoaded', function() {
    const formInputs = document.querySelectorAll('.form__field .form__input');

    formInputs.forEach(input => {

        input.addEventListener("focus", () => {
            const formGroup = input.closest('.form__field');

            formGroup.classList.add(focusClass); 
        });

        input.addEventListener("blur", () => {
            let inputValue = input.value;

            if (inputValue == '' ) {
                const formGroup = input.closest('.form__field');
                formGroup.classList.remove(focusClass);
            }
        });
    });
});