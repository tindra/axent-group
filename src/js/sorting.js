let $sortingToggle = $('[data-element="sorting-toggle"]');

$sortingToggle.on('click', function(e) {
    e.preventDefault();

    let $toggle = $(this);
    let $select = $('[data-element="select"][data-id="'+$toggle.attr('data-id')+'"]');

    $select.select2({
        dropdownParent: $toggle.parent(),
        dropdownCssClass: ':all:',
        containerCssClass: ':all:',
        minimumResultsForSearch: Infinity,
        width: 223,
    });

    $select.on('select2:select', function (e) {
        $body.removeClass(sortingSelectVisible);
    });

    if ($body.hasClass(sortingSelectVisible)) {
        $body.removeClass(sortingSelectVisible);
        $select.select2('close');
    } else {
        $body.addClass(sortingSelectVisible);
        $select.select2('open');
    }
});