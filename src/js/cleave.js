/* Cleave inputs */
(function() {
    let $phoneInputs = document.querySelectorAll('[data-element="phone-input"]');

    if ($phoneInputs.length === 0) {
        return false;
    }

    for (const input of $phoneInputs) {
        let phoneCleave = new Cleave(input, {
            delimiters: ['+7 (', ')', ' ', '-', '-'],
            blocks: [0, 3, 0, 3, 2, 2],
            numericOnly: true
        });
    }
})();