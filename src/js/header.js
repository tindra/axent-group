/* Header on scroll */
document.addEventListener('DOMContentLoaded', function() {
    (function () {
        const $header = document.querySelector('[data-component="header"]');
        const headerHeight = $header.offsetHeight;
        const $body = document.querySelector('.body');

        if (!$header) {
            return false;
        }

        let onScroll = debounce(function () {
            $header.classList.toggle(scrolledClass, window.scrollY > headerHeight);
            $body.classList.toggle(headerScrolledClass, window.scrollY > headerHeight);
        }, 0);

        onScroll();
        window.addEventListener('scroll', function() {
            onScroll();
        });
    })();
});