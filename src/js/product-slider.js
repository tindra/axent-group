/* Product slider */
$(document).ready(function(){
    let $productSlider = $('[data-slider="product"]');

    $productSlider.slick({
        infinite: false,
        asNavFor: '[data-slider="product-thumbs"]',
    });

    let $productThumbs = $('[data-slider="product-thumbs"]');
    $productThumbs.slick({
        infinite: false,
        vertical: true,
        verticalSwiping: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        focusOnSelect: true,
        asNavFor: '[data-slider="product"]',
    });

    $(window).on('orientationchange resize', function() {
        $productSlider.slick('resize');
        $productThumbs.slick('resize');
    });
});