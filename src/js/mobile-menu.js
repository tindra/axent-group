/* Mobile menu */
let $mobileMenu = $('[data-component="mobile-menu"]');
let $mobileMenuToggle = $('[data-element="mobile-menu-toggle"]');
let $mobileMenuClose = $('[data-element="mobile-menu-close"]');

function hideMobileMenu() {
    $mobileMenu.removeClass(visibleClass);
    $mobileMenuToggle.removeClass(activeClass);

    if ($body.hasClass(mobileMenuVisibleClass)) {
       $body.removeClass(lockedScrollClass);
       $body.removeClass(mobileMenuVisibleClass);
       $body.removeClass(subnavVisibleClass);
    }
}

function showMobileMenu(component) {
    if ($(window).width() < bpXL) {
        component.addClass(visibleClass);
        $mobileMenuToggle.addClass(activeClass);
        $body.addClass(mobileMenuVisibleClass);
        $body.addClass(lockedScrollClass);
    }
}

function desktopCheck() {
    if ($(window).width() >= bpXL) {
        hideMobileMenu();
    }
}

$(document).ready(function(){

    $mobileMenuToggle.on('click', function(e) {
        e.preventDefault();

        let $toggle = $(this);
        let $component = $('[data-component="mobile-menu"]');

        if ($component.hasClass(visibleClass)) {
            hideMobileMenu();
        } else {
            $('[data-component="mobile-menu"]').removeClass(visibleClass);
            showMobileMenu($component);
        }
    });

    $mobileMenuClose.on('click', function(e) {
        e.preventDefault();

        hideMobileMenu();
    });

    $(document).on('keyup', function(e) {
        if ($(window).width() < bpXL) {
            if (e.key === "Escape" || e.keyCode === 27) {
                hideMobileMenu();
            }
        }
    });

    $(document).on('click', function(e) {
        if ($(window).width() < bpXL) {
            if ($mobileMenu.has(e.target).length === 0 && !$mobileMenu.is(e.target) && $mobileMenuToggle.has(e.target).length === 0 && !$mobileMenuToggle.is(e.target) && $('[data-element="nav-close"]').has(e.target).length === 0 && !$('[data-element="nav-close"]').is(e.target)) {
                hideMobileMenu();
            }
        }
    });

    desktopCheck();

    $(window).on('orientationchange resize', function() {
        desktopCheck();
    }); 
});