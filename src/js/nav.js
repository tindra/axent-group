/* Nav */
$(function() {
    let $navToggle = $('[data-element="nav-toggle"]');
    let $navItems = $('[data-element="nav-item"]');
    let $navClose = $('[data-element="nav-close"]');

    $navClose.on('click', function(e){
        e.preventDefault();

        $navItems.removeClass(activeClass);
        $navItems.show();

        $body.removeClass(subnavVisibleClass);
    });

    navResponsive();
    $(window).on('orientationchange resize', function() {
        navResponsive();
    });

    function navResponsive() {
        if ($(window).width() < bpXL) {
            $navToggle.on('click', function(e){
                e.preventDefault();

                let $toggle = $(this);
                let $item = $toggle.closest('[data-element="nav-item"]');
                let $component = $toggle.closest('[data-component="nav"]');

                $item.toggleClass(activeClass);
                $item.siblings().toggle();

                $body.toggleClass(subnavVisibleClass);
            });
        } else {
            $navItems.removeClass(activeClass);
            $navItems.show();
        }
    }
});