/* Datepicker */
document.addEventListener('DOMContentLoaded', function() {
    let $datePickers = document.querySelectorAll('[data-element="datepicker"]');

    if ($datePickers) {
        for (var i = 0; i < $datePickers.length; i++) {
            let picker = datepicker($datePickers[i], {
                position: 'bl',
                startDay: 1,
                showAllDates: true,
                customDays: ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
                customOverlayMonths: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Aвг', 'Сен', 'Окт', 'Ноя', 'Дек'],
                customMonths: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Aвгуст', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                overlayButton: 'Выбрать',
                overlayPlaceholder: 'Год',
                formatter: (input, date, instance) => {
                    const value = date.toLocaleDateString();
                    input.value = value;
                },
                onSelect: (instance, date) => {
                    $(instance.el).parsley().validate();
                },
            });
        }
    }
});