/* Filters */
let $filters = $('[data-component="filters"]');
let $filtersTrigger = $('[data-element="filters-trigger"]');
let $filtersClose = $('[data-element="filters-close"]');

function hideFilters() {
    $filters.removeClass(visibleClass);
    $body.removeClass(lockedScrollClass);
}

function showFilters(component) {
    if ($(window).width() < bpXL) {
        component.addClass(visibleClass);
        $body.addClass(lockedScrollClass);
    }
}

function desktopCheck() {
    if ($(window).width() >= bpXL) {
        hideFilters();
    }
}

$(document).ready(function(){

    $filtersTrigger.on('click', function(e) {
        e.preventDefault();

        let $trigger = $(this);
        let $component = $('[data-component="filters"]');

        if ($component.hasClass(visibleClass)) {
            hideFilters();
        } else {
            showFilters($component);
        }
    });

    $filtersClose.on('click', function(e) {
        e.preventDefault();

        hideFilters();
    });

    $(document).on('keyup', function(e) {
        if ($(window).width() < bpXL) {
            if (e.key === "Escape" || e.keyCode === 27) {
                hideFilters();
            }
        }
    });

    $(document).on('click', function(e) {
        if ($(window).width() < bpXL) {
            if ($filters.has(e.target).length === 0 && !$filters.is(e.target) && $filtersTrigger.has(e.target).length === 0 && !$filtersTrigger.is(e.target)) {
                hideFilters();
            }
        }
    });

    desktopCheck();

    $(window).on('orientationchange resize', function() {
        desktopCheck();
    }); 
});